from datetime import datetime

def all_permutations(string):
    """
    Generates all permutations of a given string.
    
    Args:
        string: The input string.
    
    Yields:
        Strings: Each permutation of the input string.
    """
    if len(string) <= 1:
        yield string
    else:
        for first_char in string:
            remaining_chars = string.replace(first_char, '', 1)
            for permutation in all_permutations(remaining_chars):
                yield first_char + permutation

def all_permutations_rust(string):
    return []

if __name__ == '__main__':
    s = 'qwertyuiop' 
    start_py = datetime.now()
    res_py = list(all_permutations(s))
    end_py = datetime.now()
    
    start_rs = datetime.now()
    res_rs = all_permutations_rust(s)
    
    end_rs = datetime.now()
    # assert len(res_py) == len(res_rs)
    
    print(f'Python run in :{end_py - start_py}')
    print(f'Rust run in   :{end_rs - start_rs}')
    