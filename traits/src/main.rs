// mod  generics;
// mod loops;

struct Tetragon {
    side1: u32,
    side2: u32,
    side3: u32,
    side4: u32
}

struct Square {
    side: u32
}

// #[derive(PartialEq, Debug)]
struct Rectangle {
    width: u32,
    height: u32
}

trait Perimeter {
    fn perimeter(&self) -> u32;
}



fn main() {
    // let tetra = Tetragon::new(1,2,3,4);
    // assert_eq!(tetra.perimeter, 10);
    
    // let square = Square::default();
    // assert_eq!(square.perimeter(), 4)
    
    // let rect = Rectangle::default();
    // assert_eq!(rect.perimeter(), 6);
    
    // assert_eq!(Square {side: 2} * 3_u32, Rectangle {width: 6, height: 2});
    
    // generics
    // let a_i32 = generics::add_generics(32_i32, 34_i32);
    // let a_i64 = generics::add_generics(32_i64, 34_i64);
    // let a_u16 = generics::add_generics(32_u16, 34_u16);
    
    // iter trait
    // assert_eq!(loops::square_elements_loop(vec![1,3,10]), vec![1, 9, 100]);
    // assert_eq!(loops::square_elements_iter(vec![1,3,10]), vec![1, 9, 100]);


    // complexe:
    // create the function square_elements_generics that takes any iterable struct with any multiplicative (with copy trait too) as item
    
    // create a trait DisplayElems that have the function display_elems that display each element and continue
    let v =  vec![1,3,4];
    // for elem in v.into_iter().display_elems() {
    //     elem + elem;
    // }
}
