pub fn var_mut() {
    let x: i32 = 5;
    // all var by default are immutable
    x = -5;
    println!("success, x = {}", x)
}

pub fn var_cast() {
    let x = 10000_u32;
    // mutiple solutions here one way is to use the keyword as
    let y: i32 = x;
}

pub fn var_context() {
    let x = {
      let a = 5;
      a*a  
    };
    println!("{} squared = {}", a, x);
}