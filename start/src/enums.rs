
enum TrafficLight {
    Green(u8),
    Orange,
    Red(u8)
}

pub fn ex_match() {
    let light = TrafficLight::Red(6);
    match light {            
        TrafficLight::Orange => println!("wait 5 secs"),
        TrafficLight::Green(a) => println!("{} sec for ", a),
        _ => {}
  }
}

#[derive(Debug)]
struct Person {
    name: String,
    age: u8
}

pub fn create_person() {
    let pers = Person {
        name: "toto".to_string(),
        age: 4  
    };
    println!("{:?}", pers);
    let other_pers = Person {
        name: "titi".to_string(),
        age: 4
    };
    if pers == other_pers {
        println!("same person")
    } else {
        println!("not the same person")
    };
}