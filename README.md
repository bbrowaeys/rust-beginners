# rust-beginners

## 1 start folder
fix compilation error

start by uncommented mod var and var functions in main

then functions

then struct enum

## 2 pyo3 fibonacci
create a python virtual env (and activate it)
install maturin
```
pip install maturin
```
build and install rust_fibo lib with maturin
```
maturin develop
```
run fibo.py script, it should raise a not implemented error
implement rust_fibo in the lib.rs with the same algo than in python

how fast rust is in comparison to python ?

(for fun)
add two lines in the python script to improve drastically the algo

## 2' pyo3 strings
same as 2 for string permutation algo


## 3 traits

solve exercises in traits/src/main.rs

