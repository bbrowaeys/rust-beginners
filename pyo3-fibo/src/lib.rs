use pyo3::prelude::*;

#[pyfunction]
fn rust_fibo(a: u128) -> u128 {
    unimplemented!()
}



#[pymodule]
fn pyo3_fibo(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(rust_fibo, m)?)?;
    Ok(())
}