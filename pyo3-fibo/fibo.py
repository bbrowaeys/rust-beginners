from datetime import datetime
from pyo3_fibo.pyo3_fibo import rust_fibo

def fibo_python(x):
    if x == 0 or x == 1:
        return x
    return fibo_python(x - 1) + fibo_python(x - 2)

def fibo_rust(x):
    return rust_fibo(x)

if __name__ == '__main__':
    n = 35
    start_py = datetime.now()
    res_py = fibo_python(n)
    end_py = datetime.now()
    
    start_rs = datetime.now()
    res_rs = fibo_rust(n)
    
    end_rs = datetime.now()
    assert res_py == res_rs
    
    print(f'Python run in :{end_py - start_py}')
    print(f'Rust run in   :{end_rs - start_rs}')
    
    

